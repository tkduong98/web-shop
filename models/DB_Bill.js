const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const billSchema = new Schema({
    ten_kh:String,
    sdt_kh:String,
    diachinhan_hd: String,
    tonggia_hd: Number,
    ghichu_hd: String,
    time_hd: Date
});
const Bill = mongoose.model('tb_hoadon',billSchema);

module.exports = Bill;