const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const detailBill = new Schema({
    soluong: Number,
    image:String,
    mau:String,
    dongia: Number,
    ten:String,
    id_sp: { type: Schema.Types.ObjectId, ref: "tb_sanpham" }
});

const DetailBill = mongoose.model('tb_chitiethoadon',detailBill);

module.exports = DetailBill;