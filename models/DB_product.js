const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//connect

const productSchema = new Schema({
    ten_sp : String,
    anh_sp : String,
    gia_sp : Number,
    soluong_sp : Number,
    mota_sp : String,
    id_dm : {type: mongoose.Schema.Types.ObjectId,ref:"tb_danhmucsp"}

},{autoIndex:false});

const Product = mongoose.model('tb_sanpham',productSchema);

module.exports = Product;