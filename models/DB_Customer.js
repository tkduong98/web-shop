const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const customerSchema = new Schema({
    ten_kh: String,
    sdt_kh:Number,
    email_kh:String,
    id_hd:[{type:Schema.Types.ObjectId,ref:"tb_hoadon"}]
},{autoIndex:false});

const Customer = mongoose.model('tb_khachhang',customerSchema);

module.exports = Customer;