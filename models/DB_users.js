const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// schema
const detailBill = new Schema({
    soluong: Number,
    image:String,
    mau:String,
    dongia: Number,
    ten:String,
    id_sp: { type: Schema.Types.ObjectId, ref: "tb_sanpham" }
});
const userSchema = new Schema({
    email_nd: String,
    taikhoan_nd: String,
    matkhau_nd: String,
    cart_nd: [detailBill]

}, { autoIndex: false });

const user = mongoose.model('tb_nguoidung', userSchema);
module.exports = user;