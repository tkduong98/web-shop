const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const bodyparser = require('body-parser');
server.listen(process.env.PORT || 4000);
const bcrypt = require('bcryptjs');
const saltRounds = 10;
const passport = require('passport');
const session = require('express-session');
const localStrategy = require('passport-local').Strategy;
var forgetpass = require('./routes/forgot-pass');
const mongoose = require('mongoose');
app.use(session({
    secret: 'dsahjkdsa',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 1000 * 60 * 60 * 24 }
}));
app.use('/forgotpassword', forgetpass);
app.use(express.static("./public"));
app.use(passport.initialize());// khoi tao passport
app.use(passport.session());
app.use(bodyparser.urlencoded({ extended: true }));
app.set("view engine", "ejs");
app.set("views", "./views");

mongoose.connect('mongodb://admin:sonlong123@ds161134.mlab.com:61134/demo_shop',{ useNewUrlParser: true }, (err, rs) => {
    if (!err) console.log('connect success DB Bill !');
    else return err;
});
const User = require('./models/DB_users');
const Product = require('./models/DB_product');
const Category = require('./models/DB_category');
const Bill = require('./models/DB_Bill');
// const Customer = require('./models/DB_Customer');
const Detailbill = require('./models/DB_detailBill');
// const Saler = require('./models/DB_saler');


app.get("/", (req, res) => {
    if (req.isAuthenticated()) {
        Product.find().limit(4).exec((err, rs) => {
            Product.find({ id_dm: "5c499108e7179a544942689a" }).limit(4).exec((err, lap) => {
                Product.find({ id_dm: "5c49af11e7179a5449427a24" }).limit(4).exec((err, ipad) => {
                    res.render('index', { rs: rs, lap: lap, ipad: ipad });
                })

            })
        })
    } else {
        console.log('chua login');
        res.render('login');
    }
});
app.route('/login')
    .get((req, res) => {
        if (req.isAuthenticated()) {
            res.redirect('/index');
        } else {
            console.log('chua login');
            res.render('login');
        }
    })
    .post(passport.authenticate('local', {
        successRedirect: '/index',
        failureMessage: "Khong ton tai",
        failureRedirect: '/login'
    }));// chung thuc
// bcrypt.hash("123456",saltRounds,(er,hash)=>{
//     console.log('bcrypt: '+ hash);
//     bcrypt.compare('123456',hash,(err,rs)=>{
//         console.log(rs);
//     })
// });
passport.use(new localStrategy(
    (username, password, done) => {
        User.find().exec((err, rs) => {
            const userRecord = rs.find(u => u.taikhoan_nd == username);

            if (userRecord) {
                bcrypt.compare(password, userRecord.matkhau_nd, (err, kq) => {
                    if (kq === true) {
                        console.log('Ton Tai user : ' + userRecord.taikhoan_nd);
                        return done(null, userRecord);
                    } else {
                        console.log('sai Mat khau');
                        return done(null, false);
                    }
                })
            } else {
                console.log('k ton tai ');
                return done(null, false);
            }
        });
    }
));
passport.serializeUser((user, done) => { // luu gia tri vao cookie
    done(null, user.taikhoan_nd);
});
passport.deserializeUser((name, done) => { //lay gia tri da luu trong cookie
    User.find().exec((err, rs) => {
        const userRecord = rs.find(u => u.taikhoan_nd == name);
        if (userRecord) {
            return done(null, userRecord);
        } else {
            return done(null, false);
        }
    });
});
// User.findOne({},(err,rs)=>{
//     rs.cart_nd.push(obj);
//     rs.save((err,rs)=>{
//         console.log('da them')
//     })
// })

io.on('connection', (socket) => {
    console.log('co nguoi ket noi socket ');
    socket.on('datacart', () => {
        // Detailbill.find().exec((err, cart) => {
        //     socket.emit('cart', { sl: cart.length, cart: cart });
        // })
    })
    socket.on('data', (data) => {
        let obj = new Detailbill({
            soluong: data.pQuality,
            image: data.pImage,
            mau: data.pColor,
            dongia: data.pPrice,
            ten: data.pName,
            id_sp: data.pID,
            user: data.u
        });
        User.findOne({ taikhoan_nd: data.u }, (err, rs) => {
            rs.cart_nd.push(obj);
            rs.save((err, rs) => {
                if (!err) console.log('da them vao CArt ');
                socket.emit('cart', { sl: rs.cart_nd.length, cart: rs.cart_nd });
            })
        })
    });
    socket.on('email-client', (data) => {
        User.findOne({ email_nd: data }, (err, rs) => {
            if (rs) {
                socket.emit('email-exist');
            } else {
                socket.emit('email-can-use');
            }
        })
    });
    socket.on('username-client', (data) => {
        User.findOne({ taikhoan_nd: data }, (err, rs) => {
            if (rs) {
                socket.emit('username-exist');
            } else {
                socket.emit('username-can-use');
            }
        })
    })
    socket.on('pswrepeat', (data) => {
        if (data.psw1 == data.psw2) {
            socket.emit('pws-success');
        } else {
            socket.emit('pws-fail');
        }
    })
    app.post('/register', (req, res) => {
        var email = req.body.email;
        var username = req.body.username;
        var pws = req.body.pws;
        var pwsrepeat = req.body.pwsrepeat;
        User.findOne({ email_nd: email }, (err, rs) => {
            User.findOne({ taikhoan_nd: username }, (err, rs1) => {
                if (rs || rs1 || pws != pwsrepeat) {
                    io.emit('register-fail');
                } else {
                    bcrypt.hash(pws, saltRounds, (err, hash) => {
                        User.create({
                            email_nd: email,
                            taikhoan_nd: username,
                            matkhau_nd: hash,
                            cart_nd: []
                        }, (er, rs) => {
                            if (!er) {
                                console.log('da them vao DB');
                                io.emit('register-success');
                                setTimeout(() => {
                                    res.redirect('login');
                                }, 3000);
                            }
                        })
                    })
                }
            })
        })
    });
});
app.get('/logout', (req, res) => {
    User.findOne({ taikhoan_nd: req.session.passport.user }, (err, rs) => {
        rs.set({ cart_nd: [] });
        rs.save((er, rs) => {
            console.log('cleaned cart !');
        });
    })
    req.logout();
    res.redirect('/');
})
app.get('/register', (req, res) => {
    res.render('register');
})
app.get('/index', (req, res) => {
    //do something only if user is authenticated
    console.log('loged in');
    if (req.isAuthenticated()) {
        Product.find({ id_dm: "5c4990f2e7179a544942688f" }).limit(4).exec((err, rs) => {
            Product.find({ id_dm: "5c499108e7179a544942689a" }).limit(4).exec((err, lap) => {
                Product.find({ id_dm: "5c49af11e7179a5449427a24" }).limit(4).exec((err, ipad) => {
                    User.findOne({ taikhoan_nd: req.session.passport.user }, (err, cart) => {
                        var tongtien = 0;
                        cart.cart_nd.forEach(u => {
                            tongtien += u.dongia * u.soluong;
                        })
                        res.render('index', { rs: rs, lap: lap, ipad: ipad, sl: cart.cart_nd.length, sum: tongtien });
                    })

                })
            })
        })
    } else {
        console.log('chua login');
        res.redirect('/login');
    }
});
app.get('/allproduct', (req, res) => {
    if (req.isAuthenticated()) {
        Product.find().exec((err, rs) => {
            User.findOne({ taikhoan_nd: req.session.passport.user }, (err, cart) => {
                var tongtien = 0;
                cart.cart_nd.forEach(u => {
                    tongtien += u.dongia * u.soluong;
                })
                res.render('allproduct', { rs, sl: cart.cart_nd.length, sum: tongtien });
            });
        })
    } else {
        console.log('chua login');
        res.render('login');
    }
});
app.get('/checkout', (req, res) => {
    if (req.isAuthenticated()) {
        User.findOne({ taikhoan_nd: req.session.passport.user }, (err, cart) => {
            var tongtien = 0;
            cart.cart_nd.forEach(u => {
                tongtien += u.dongia * u.soluong;
            })
            res.render('checkout', { sl: cart.cart_nd.length, sum: tongtien, cart: cart.cart_nd, user: req.session.passport.user });
        });
    } else {
        console.log('chua login');
        res.render('login');
    }

})
app.get('/checkout/:id', (req, res) => {
    if (req.isAuthenticated()) {
        var id_xoa = req.params.id;
        console.log(typeof (id_xoa));
        User.findOne({ taikhoan_nd: req.session.passport.user }, (err, rs) => {
            var n = rs.cart_nd.length;
            var vt = 0;
            for (var i = 0; i < n; i++) {
                if (String(rs.cart_nd[i]._id) == id_xoa) {
                    vt = i;
                }
            }
            rs.cart_nd.splice(vt, 1);
            rs.save((err, rs) => {
                if (!err) {
                    res.redirect('/checkout');
                }
            })
        })
    } else {
        console.log('chua login');
        res.redirect('/login');
    }
})
app.get('/checkout2', (req, res) => {
    if (req.isAuthenticated()) {
        User.findOne({ taikhoan_nd: req.session.passport.user }, (err, cart) => {
            var tongtien = 0;
            cart.cart_nd.forEach(u => {
                tongtien += u.dongia * u.soluong;
            })
            res.render('checkout2', { sl: cart.cart_nd.length, sum: tongtien, time: new Date() });
        });
    } else {
        console.log('chua login');
        res.render('login');
    }
})
app.post('/checkout2', (req, res) => {
    let ten = req.body.ten;
    let sdt = req.body.sdt;
    let diachi = req.body.diachi;
    let tongtien = req.body.tongtien;
    let ghichu = req.body.ghichu;
    Bill.create({
        ten_kh: ten,
        sdt_kh: sdt,
        diachinhan_hd: diachi,
        tonggia_hd: parseInt(tongtien),
        ghichu_hd: ghichu,
        time_hd: new Date
    }, (er, rs) => {
        if (!er) {
            if (req.isAuthenticated()) {
                User.findOne({ taikhoan_nd: req.session.passport.user }, (err, cart) => {
                    cart.set({ cart_nd: [] });
                    cart.save((er, rs) => {
                        if (!er) {
                            var tongtien = 0;
                            cart.cart_nd.forEach(u => {
                                tongtien += u.dongia * u.soluong;
                            })
                            res.render('checkout2', { sl: cart.cart_nd.length, sum: tongtien });
                        }
                        console.log('cleaned cart !');
                    });
                })
            } else {
                console.log('chua login');
                res.render('login');
            }
        }
    })
})
app.get('/mobile', (req, res) => {
    if (req.isAuthenticated()) {
        Product.find({ id_dm: "5c4990f2e7179a544942688f" }).exec((err, rs) => {
            User.findOne({ taikhoan_nd: req.session.passport.user }, (err, cart) => {
                var tongtien = 0;
                cart.cart_nd.forEach(u => {
                    tongtien += u.dongia * u.soluong;
                })
                res.render('allproduct', { rs, sl: cart.cart_nd.length, sum: tongtien });
            });
        })
    } else {
        console.log('chua login');
        res.render('login');
    }

})
app.get('/laptop', (req, res) => {
    if (req.isAuthenticated()) {
        Product.find({ id_dm: "5c499108e7179a544942689a" }).exec((err, rs) => {
            User.findOne({ taikhoan_nd: req.session.passport.user }, (err, cart) => {
                var tongtien = 0;
                cart.cart_nd.forEach(u => {
                    tongtien += u.dongia * u.soluong;
                })
                res.render('allproduct', { rs, sl: cart.cart_nd.length, sum: tongtien });
            });
        })
    } else {
        console.log('chua login');
        res.render('login');
    }

})
app.get('/ipad', (req, res) => {
    if (req.isAuthenticated()) {
        Product.find({ id_dm: "5c49af11e7179a5449427a24" }).exec((err, rs) => {
            User.findOne({ taikhoan_nd: req.session.passport.user }, (err, cart) => {
                var tongtien = 0;
                cart.cart_nd.forEach(u => {
                    tongtien += u.dongia * u.soluong;
                })
                res.render('allproduct', { rs, sl: cart.cart_nd.length, sum: tongtien });
            });
        })
    } else {
        console.log('chua login');
        res.render('login');
    }

})
app.get('/delivery', (req, res) => {
    if (req.isAuthenticated()) {
        User.findOne({ taikhoan_nd: req.session.passport.user }, (err, cart) => {
            var tongtien = 0;
            cart.cart_nd.forEach(u => {
                tongtien += u.dongia * u.soluong;
            })
            res.render('delivery', { sl: cart.cart_nd.length, sum: tongtien });
        });
    } else {
        console.log('chua login');
        res.render('login');
    }

})
app.get('/about', (req, res) => {
    if (req.isAuthenticated()) {
        User.findOne({ taikhoan_nd: req.session.passport.user }, (err, cart) => {
            var tongtien = 0;
            cart.cart_nd.forEach(u => {
                tongtien += u.dongia * u.soluong;
            })
            res.render('about', { sl: cart.cart_nd.length, sum: tongtien });
        });
    } else {
        console.log('chua login');
        res.render('login');
    }

})
app.get('/contact', (req, res) => {
    if (req.isAuthenticated()) {
        User.findOne({ taikhoan_nd: req.session.passport.user }, (err, cart) => {
            var tongtien = 0;
            cart.cart_nd.forEach(u => {
                tongtien += u.dongia * u.soluong;
            })
            res.render('contact', { sl: cart.cart_nd.length, sum: tongtien });
        });
    } else {
        console.log('chua login');
        res.render('login');
    }

})
app.get('/news', (req, res) => {
    if (req.isAuthenticated()) {
        User.findOne({ taikhoan_nd: req.session.passport.user }, (err, cart) => {
            var tongtien = 0;
            cart.cart_nd.forEach(u => {
                tongtien += u.dongia * u.soluong;
            })
            res.render('news', { sl: cart.cart_nd.length, sum: tongtien });
        });
    } else {
        console.log('chua login');
        res.render('login');
    }

})
app.get('/:id', (req, res) => {
    if (req.isAuthenticated()) {
        id = req.params.id;
        Product.findOne({ _id: id }).exec((err, rs) => {
            Product.find().limit(4).exec((err, lq) => {
                User.findOne({ taikhoan_nd: req.session.passport.user }, (err, cart) => {
                    var tongtien = 0;
                    cart.cart_nd.forEach(u => {
                        tongtien += u.dongia * u.soluong;
                    })
                    res.render('preview', { sl: cart.cart_nd.length, sum: tongtien, rs: rs, lq: lq, u: req.session.passport.user });
                });
                // res.render('preview', { rs: rs, lq: lq , u:req.session.passport.user });
            })

        })
    } else {
        console.log('chua login');
        res.render('login');
    }
})
